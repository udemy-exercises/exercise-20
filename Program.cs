﻿using System;

class Program
{
   static void Main()
   {
      Console.WriteLine("Enter contract data");

      Console.Write("Number: ");
      int number = int.Parse(Console.ReadLine());

      Console.Write("Date (MM/dd/yyyy): ");
      DateOnly date = DateOnly.Parse(Console.ReadLine());

      Console.Write("Contract value: ");
      double totalValue = double.Parse(Console.ReadLine());

      Console.Write("Number of installments: ");
      int amountInstallments = int.Parse(Console.ReadLine());

      var contract = new Contract(number, date, totalValue, amountInstallments);

      Console.WriteLine("Installments: ");
      for (int i = 1; i < contract.installments.Length +1; i++)
      {
         date = date.AddMonths(1);
         Console.WriteLine($"{date} - ${(contract.totalValue / contract.installments.Length / 100 * i) + ((contract.totalValue / contract.installments.Length) / 50) + (contract.totalValue / contract.installments.Length)}");
      }
   }
}