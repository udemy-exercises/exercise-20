using System;

class Contract
{
   public int number;
   public DateOnly date;
   public double totalValue;
   public Installment[] installments;

   public Contract(int number, DateOnly date, double totalValue, int amountInstallments)
   {
      this.number = number;
      this.date = date;
      this.totalValue = totalValue;
      this.installments = new Installment[amountInstallments];
   }
}